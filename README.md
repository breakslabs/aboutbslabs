# About BSLabs

A project container for general documentation about Break Stuff Labs' code
style, tooling, and other generalities.


## General comments on code style

I write in 80-column mode whenever possible. While I realize very few people
are sitting on the action-side of a [DEC
VT-102](https://en.wikipedia.org/wiki/VT100) terminal these days and display
resolutions have exceeded the absurd, I still think the 80-column soft-stop
serves a few useful purposes:


- In spite of the fact that modern displays allow for gigantic text column
  counts, a great many people prefer not to do our work in a full-width
  window. Some of that real-estate is often better used for compiler
  outputs, documentation references, or other monitoring.
- Some of us are old. And when we're not tying an onion to our belts, we
  like BIG FONT SIZES. Laugh while you can - it'll happen to you, too.
- That soft-78 (or so) helps keep code sane. JavaScript and Lisp, for
  examples, both tend towards (huge-lines (that 'keep "going" (for (days
  because we can) and it seemed) like a good-idea) at the time). Those are
  horrible things to debug. Admittedly this can be difficult when URLs and
  GUIDs span for days, and variables and functions/methods are meant to be
  descriptive. 
- It gives people something to argue about.


With all that, I prefer descriptive, but not intrusive, symbols. I grew up in
an age and environment where variables names were actually limited to no more
than two characters, and functions didn't exist. The ability to name things
descriptively is a luxury and a boon. At the same time, name your iterator 'i'
or 't' or something. There's no need to turn it into an obsession.


On the topic of naming, I tend to prefer so-called snake_case, but will
happily produce camelCase, PascalCase, or kebab-case if that is the common
style for a given language or project. When in Rome and all that. But I will
never do sTUdlY CAsE - I had enough of that many years ago. And absolutely no
cAPS lOCK cASE, please.


## General computing philosophy

I believe in several things that have variously been referred to as the "UNIX
philosophy" or similar. Namely the following:

- Text is king. Human-readable data that can be hand edited makes all our
  lives much easier. It also allows common tooling; Processes that
  understand text-in and produce text-out can be chained together like
  LEGOs to produce programs greater than the sum of their parts. This
  leads to:
- Programs should generally Do One Thing, and preferably do it well. Web
  browsers don't need to talk SMTP, and every piece of software shouldn't
  be creating its own database format. I admit that I do use Emacs, but
  I'll leave arguments as to why that doesn't violate the Do One Thing
  rule as an exercise for the reader.
- Everything is a file (except when it's not). This is the glue that binds
  the first and second rules together. Make as many things behave like a
  file as make sense, and you have one interface to learn. With STDIN and
  STDOUT as files, the idea of a pipe comes almost naturally, and the flow
  of data is easier to follow.


Having said that, no bugbears. Sometimes it makes sense to have binary
data. Sometimes it makes sense to throw the kitchen sink in there. And
sometimes things just aren't files. I also know that the UNIX world took an
awful long time to come to internationalization, and in a lot of ways is still
very ASCII-centric. Still, any time I find myself preparing to ignore one of
these three points, I find it's a really good idea to take a step back and
have a good, hard look at what I'm doing, first. Sometimes there's a simpler,
more elegant solution. Sometimes the problem didn't even need to be solved.

## A bit about me

I've been fussing with technology since I was old enough to manipulate a
screwdriver. Unfortunately, I've been slack at putting that to good use. Sure,
there's my day job (which I haven't quit, I promise), but some things went
sideways, and I fulfilled several teachers' prognostications by completely
failing to live up to my potential. This whole mess is an attempt at
correcting some of that. I've got a lot of homework to make up.


When it comes to languages, I have a fairly good grip on C, Python, ECMAScript
C++ (although I'm way out of date by modern standards), and to a lesser extent
Lisp. I've dabbled in Rust, Forth, Pascal, something called 'E' (don't ask),
and probably half a dozen others I can't think of right now. I can write a
fairly ugly regular expression, and can shoot myself in the foot with BASH any
time you like. On top of all that, I was once a fairly handy 6502/6510 machine
code guy, should anyone need a program for 1983. Oh, yeah - I got that. That
in mind, I'm an autodidact, and I started out on BASIC (kill it with fire). I
have some horrible habits, and certainly don't know the best way to do
everything. I gave up my Rockstar badge years ago - if you see something, say
something. I'm always ready to discuss, and I love to learn.


I prefer to use free (as in speech) software, and will do so whenever
possible. I have contributed to a few such projects, but it's been many
years. But I'm politically motivated in that arena, as well as financially. I
think closed-source and patent-encumbered software is a problem, and am always
looking at solutions. I'm not a fanatic, but I can reach out and pinch a few
from where I sit.

As far as any forum where I wield veto power (my channels, issue trackers,
social accounts, etc.), I have a twitchy ban-hammer. Don't be a bigot, racist,
sexist, or really most any kind of '-ist' (dentists are okay), and we'll
probably get along fine. I've been interacting with people online since before
AOL was Quantum Link, and I know what unfettered interaction dissolves to. I'll
defend anyone's right to be free of government censorship - even those I
disagree with - but I'm not the government. My time and my space are mine, and
I get to choose who I share it with.


## Misc

I excel at over-exposition (no way!). I understand the logical fallacy which
equates the enjoyment of big butts with the inability to deceive. I once spun
someone right 'round, baby. Right 'round. For a good time, call 606-4311. Ask
for "Ken".

## Channel

So this GitLab is attached to my YouTube channel. I try to learn new things,
and maybe take some other people along with me. Sometimes this results in
broken things and so, ever the optimist, I called it Break ~~Shit~~ Stuff
Labs. Which is an inside joke that only one other person might get. In short,
the second-best kind of inside joke. I am also not unaware of the colloquial
meaning of "BS". 

If you want, you can check out the video stuff at:

https://www.youtube.com/channel/UCyCYrP3dijCirFRAZ9WcGNQ

Someday I hope to become a real boy and maybe have one of those slick vanity
URLs. Oh, we'll see...

